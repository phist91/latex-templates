# LaTeX-Vorlagen

Dies ist ein Git-Repository mit einigen LaTeX-Vorlagen, die ich im Laufe der Zeit erstellt oder abgeändert habe.
Sie können frei verwendet, weitergegeben oder angepasst werden.
[Hier geht's zum Download!](https://gitlab.com/phist91/latex-templates/-/archive/master/latex-templates-master.zip)

Ich bin dankbar für Verbesserungsvorschläge jeglicher Art, einen LaTeX-Support kann ich jedoch nicht anbieten.
Daher verweise ich auf:

* gängige Suchmaschinen
* die [TeX-Community von StackExchange](https://tex.stackexchange.com/)
* [Detexify](https://detexify.kirelabs.org/classify.html)

Phil Steinhorst <br/>
https://gitlab.com/phist91/latex-templates


## Vorhandene Vorlagen
* `abgaben-listings`: Informatik-Übungszettelabgaben mit dem `listingsutf8`-Paket.
* `abgaben-minted`: Informatik-Übungszettelabgaben mit dem `minted`-Paket.
* `thesis-template`: Einfache Vorlage für Abschlussarbeiten im Fach Informatik.
* `slides-unims`: Vorlage für Präsentationsfolien im Uni-Münster-Design.
* `how-to-tex`: Minimale LaTeX-Einführung, gehalten im Rahmen der Vorlesung *Informatik I* im Wintersemester 2017/18 (wird nicht aktualisiert).
* `uebungszettel`: Alte Übungszettelvorlage aus dem Sommersemester 2020; seitdem nicht mehr gepflegt.

## Weitere Vorlagen für Abschlussarbeiten
Als deutlich umfangreichere Vorlage für Abschlussarbeiten kann ich [Clean Thesis von Ricardo Langner](http://cleanthesis.der-ric.de/) empfehlen.
Ich selbst habe diese in leicht abgeänderter Form für meine [Masterarbeit](https://gitlab.com/phist91/gc-thesis) im Fach Informatik verwendet.
Weitere Vorlagen findet man außerdem auf [Overleaf](https://www.overleaf.com/latex/templates/tagged/thesis).
