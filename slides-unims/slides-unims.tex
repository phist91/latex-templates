%!TEX TS-program = pdflatex
%!TEX TS-options = -shell-escape
\documentclass[t,
  10pt,
  aspectratio=169,
  %handout,
]{beamer}

\usepackage[themecolor=Lapis,
  %unilogo=img/unimslogo.pdf,
  %institutelogo=img/lc.pdf,
  %claim=img/unimsclaim-de.pdf,
  %nofooterbars,
  lang=de,
  code=minted,
  bib=biber,
  bibfile=refs.bib,
  intermediatetoc,
  math
]{unimsstyle}


\author{Phil Steinhorst}
\title[Vorlage für LaTeX-Präsentationen]{Eine Vorlage für LaTeX-Präsentationen}
\subtitle{im (höchst inoffiziellen) Universität-Münster-Design}
\date{\today}


\begin{document}
\begin{frame}[plain]
  \maketitle
\end{frame}


\begin{frame}[c]{Disclaimer}
  Diese Vorlage ist im Zuge der Umbenennung der Universität Münster im September 2023 entstanden.
  Ziel der Vorlage ist optisch ansprechende und \enquote{saubere} Folien zu liefern, ohne gleichzeitig zu sehr vom Content abzulenken.
  Daher basiert sie optisch auf ehemaligen \code{wwustyle}-Vorlagen.
  
  Die Vorlage ist von mir selbst zusammengebastelt worden -- \socalled{offizielle Stellen} der Universität Münster sind daher nicht für diese verantwortlich und übernehmen auch keinen Support.
  
  \begin{flushright}
    Phil Steinhorst \\
    \url{https://gitlab.com/phist91/latex-templates}
  \end{flushright}
\end{frame}


\begin{frame}{Agenda}
  \tableofcontents[hidesubsections, hideothersubsections]
\end{frame}


\section{Verwendung und Paketoptionen}
\begin{frame}[fragile]{Einbinden des Pakets}
  Zur Verwendung der Vorlage genügt es, die Paketdatei \code{unimsstyle.sty} in denselben Ordner wie die \code{.tex}-Dateien mit den Folieninhalten zu legen und das Paket wie gewohnt einzubinden:
  
  \begin{minted}{tex}
    \usepackage[<Optionen>]{unimsstyle}
  \end{minted}
  
  Alternativ kann die Paketdatei auch in die lokale \LaTeX-Installation eingepflegt werden.
  
  Die verschiedenen Paketoptionen folgen auf den nächsten Folien.
\end{frame}


\begin{frame}[fragile]{Paketoptionen}
  Folgende Paketoptionen sind verfügbar (der Default-Wert steht jeweils an vorderster Stelle):
  
  \begin{minted}[fontsize=\footnotesize]{text}
    themecolor=[Lapis|Pantone7462|Graphit|Pantone432|Cassis|Pantone234|Cyan|Pantone312|Ozean|Pantone315|Tuerkis|Pantone3282|Apfelgruen|Pantone369|Peridot|Pantone390|Lcinfo]
    unilogo=[none|<Dateipfad>]
    institutelogo=[none|<Dateipfad>]
    nofooterbars=[false,true]
    lang=[de|en|none]
    code=[none|listingsutf8|minted]
    bib=[none|bibtex|biber]
    bibfile=[refs.bib|<Dateipfad>]
    intermediatetoc=[false|true]
    math=[false|true]
  \end{minted}
  
  Alle Keys sind optional.
  Wird ein Key nicht aufgeführt, ist der jeweilige Default-Wert aktiv.
\end{frame}


\begin{frame}{Paketoption \code{themecolor}}
  Diese Option legt die Farbe des Foliendesigns fest; Default ist \code{Lapis}.
  Folgende Farben stehen zur Verfügung:
  
  \begin{center} \ttfamily
    \begin{tabular}{lll} \hline
      \sffamily\textbf{Option} & \sffamily\textbf{alt. Option} & \sffamily\textbf{interner Farbname} \\ \hline
      \textcolor{unimscolorlapis}{$\blacksquare$ Lapis}  & \textcolor{unimscolorlapis}{Pantone7462} & \textcolor{unimscolorlapis}{unimscolorlapis} \\ \hline
      \textcolor{unimscolorgraphit}{$\blacksquare$ Graphit}  & \textcolor{unimscolorgraphit}{Pantone432} & \textcolor{unimscolorgraphit}{unimscolorgraphit} \\ \hline
      \textcolor{unimscolorcassis}{$\blacksquare$ Cassis}  & \textcolor{unimscolorcassis}{Pantone234} & \textcolor{unimscolorcassis}{unimscolorcassis} \\ \hline
      \textcolor{unimscolorcyan}{$\blacksquare$ Cyan}  & \textcolor{unimscolorcyan}{Pantone312} & \textcolor{unimscolorcyan}{unimscolorcyan} \\ \hline
      \textcolor{unimscolorozean}{$\blacksquare$ Ozean}  & \textcolor{unimscolorozean}{Pantone315} & \textcolor{unimscolorozean}{unimscolorozean} \\ \hline
      \textcolor{unimscolortuerkis}{$\blacksquare$ Tuerkis}  & \textcolor{unimscolortuerkis}{Pantone3282} & \textcolor{unimscolortuerkis}{unimscolortuerkis} \\ \hline
      \textcolor{unimscolorapfelgruen}{$\blacksquare$ Apfelgruen}  & \textcolor{unimscolorapfelgruen}{Pantone369} & \textcolor{unimscolorapfelgruen}{unimscolorapfelgruen} \\ \hline
      \textcolor{unimscolorperidot}{$\blacksquare$ Peridot}  & \textcolor{unimscolorperidot}{Pantone390} & \textcolor{unimscolorperidot}{unimscolorperidot} \\ \hline
      \textcolor{unimscolorlcinfo}{$\blacksquare$ Lcinfo}  & \textcolor{unimscolorlcinfo}{---} & \textcolor{unimscolorlcinfo}{unimscolorlcinfo} \\ \hline
    \end{tabular}
  \end{center}
\end{frame}


\begin{frame}{Paketoption \code{themecolor}}
  Das Festlegen der Farbe wird durch Setzen des Farbalias \code{themecolor} bewerkstelligt.
  Dies beeinflusst u.\,a. folgende Teile des Designs:
  \begin{itemize}
    \item Farbe der Überschriften.
    \item Farbe der Linien auf den Folien (Titelbild, Kopfzeile und Fußzeile).
    \item Farbgebung eines Standard-Blocks (\mintinline{tex}|\begin{block} ... \end{block}|).
    \item \code{itemize}-Aufzählungssymbol.
    \item Farbliche Hervorhebung durch \mintinline{tex}|\highlight<>{...}|.
  \end{itemize}
  Durch Benutzung der Farbe \code{themecolor} kann die eingestellte Farbe auch an anderen Stellen im Dokument erfolgen, welche sich dann bei Änderung der Farbe automatisch anpassen.
\end{frame}


\begin{frame}{Paketoption \code{unilogo}}
  Mit der Option \code{unilogo} wird der Pfad zum Logo (relativ zur Hauptdatei) angegeben, das auf den Folien oben links zu finden ist.
  Default ist \code{none}, wodurch kein Logo ausgegeben wird.
  
  Das Logo der Universität Münster kann von Mitarbeitenden von der \hyper{https://sso.uni-muenster.de/intern/werkzeuge/uni-marketing/corporate-design.html}{Website des Marketings} bezogen weden.
\end{frame}


\begin{frame}{Paketoption \code{institutelogo}}
  Mit der Option \code{institutelogo} wird der Pfad zum Logo (relativ zur Hauptdatei) angegeben, das auf den Folien oben links neben dem Uni-Logo zu finden ist.
  Default ist \code{none}, wodurch kein Logo ausgegeben wird.
\end{frame}


\begin{frame}{Paketoption \code{nofooterbars}}
  Die Option \code{nofooterbars} steuert die Ausgabe der farbigen Balken in der Fußzeile der Folien.
  Default ist \code{false}, wodurch die farbigen Balken gezeichnet werden.
  Gleichwertig ist, die Option \code{nofooterbars} einfach fortzulassen.
  
  Wird die Option angegeben (gleichwertig zu \code{nofooterbars=true}), wird die Ausgabe der farbigen Balken unterdrückt.
  Autorenname und Titel der Präsentation werden weiterhin ausgegeben.
\end{frame}


\begin{frame}{Paketoption \code{claim}}
  Mit der Option \code{claim} wird der Pfad zum Logo (relativ zur Hauptdatei) angegeben, welches auf der Titelfolie unten links als Claim zu sehen ist.
  Default ist \code{none}, wodurch kein Logo ausgegeben wird.
  
  Der Claim \socalled{wissen.leben} bzw. \socalled{living.knowledge} der Universität Münster kann von Mitarbeitenden von der \hyper{https://sso.uni-muenster.de/intern/werkzeuge/uni-marketing/corporate-design.html}{Website des Marketings} bezogen weden.
\end{frame}


\begin{frame}{Paketoption \code{lang}}
  Mit der Option \code{lang} kann bestimmt werden, ob die Pakete \code{babel} und \code{csquotes} eingebunden werden sollen.
  Default ist \code{de}, wodurch beide Pakete mit Optionen für die deutsche Sprache eingebunden werden.
  
  Der Wert \code{en} verwendet Optionen für die englische Sprache.
  
  Der Wert \code{none} sorgt dafür, dass \code{babel} und \code{csquotes} nicht automatisch eingebunden werden.
\end{frame}


\begin{frame}{Paketoption \code{code}}
  Mit der Option \code{code} kann bestimmt werden, welches Paket für Syntax Highlighting von Quellcode eingebunden wird.
  Default ist \code{none}, wodurch hierfür kein Paket eingebunden wird.
  
  Die Werte \code{listingsutf8} und \code{minted} binden die gleichnamigen Pakete ein, wodurch ihre jeweiligen Befehle verfügbar werden.
  Zudem werden generische Befehle verfügbar, die mit beiden Paketen funktionieren sollten:
  \begin{itemize}
    \item \mintinline{tex}|\codeinline{...}| für Inline-Code.
    \item \mintinline{tex}|\codefromfile{...}| für Code-Einbindungen aus externen Dateien.
    \item \mintinline{tex}|\begin{codeblock} ... \end{codeblock}| für direkt eingegebene Codeblöcke (erfordert meist die Frame-Option \code{fragile}).
  \end{itemize}
  Die generischen Befehle sind (noch) recht rudimentär und daher nur für den Fall gedacht, dass das Dokument mit beiden Paketen kompatibel sein soll.
\end{frame}


\begin{frame}{Paketoption \code{bib} und \code{bibfile}}
  Mit der Option \code{bib} kann bestimmt werden, welches Backend zur Erstellung eines Literaturverzeichnisses verwendet wird.
  Default ist \code{none}, wodurch kein Backend verwendet wird.
  Weitere mögliche Werte sind \code{bibtex} und \code{biber}.
  
  Mithilfe von \code{bibfile} wird der Pfad zur Bibliographiedatei (relativ zur Hauptdatei) angegeben, welche für das Literaturverzeichnis verwendet wird und die Literaturangaben enthält.
  Default ist \code{refs.bib}.
  Ist \code{bib=none}, wird der Wert von \code{bibfile} ignoriert.
\end{frame}


\begin{frame}{Paketoption \code{intermediatetoc}}
  Mit der Option \code{intermediatetoc} kann bestimmt werden, ob bei einem Wechsel zwischen zwei \code{sections} das Inhaltsverzeichnis angezeigt werden soll.
  Default ist \code{false}, wodurch dies nicht passiert.
  Gleichwertig ist, die Option \code{intermediatetoc} einfach fortzulassen.
  
  Wird die Option angegeben (gleichwertig zu \code{intermediatetoc=true}), wird zwischen zwei \code{sections} das Inhaltsverzeichnis angezeigt und die aktuelle Section hervorgehoben.
\end{frame}


\begin{frame}{Paketoption \code{math}}
  Mit der Option \code{math} kann bestimmt werden, ob die Mathe-Pakete \code{mathtools}, \code{amssymb}, \code{amsthm} und \code{thmtools} automatisch eingebunden werden.
  Default ist \code{false}, wodurch dies nicht passiert.
  Gleichwertig ist, die Option \code{math} einfach fortzulassen.
  
  Wird die Option angegeben (gleichwertig zu \code{math=true}), werden die oben genannten Pakete eingebunden.
\end{frame}


\section{Code-Vervollständigung in TeXstudio}
\begin{frame}{Code-Vervollständigung}
  Falls die Code-Vervollständigung in TeXstudio nicht funktioniert, kann man die Codevervollständigungsdatei \code{unimsstyle.cwl} zur lokalen TeXstudio-Installation hinzufügen:
  \begin{enumerate}
    \item In TeXstuduio im Menü \textit{Help} auf \textit{Check LaTeX installation} gehen.
    \item Im angezeigten \textit{System Report} den Pfad der \textit{Setting file} ausfindig machen.
      Unter diesem Pfad sollten die Unterordner \code{completion/user} zu finden sein.
    \item Die Datei \code{unimsstyle.cwl} nach \code{completion/user} kopieren.
    \item In den Einstellungen von TeXstudio unter \textit{Completion} in der umfassenden Liste die Datei \code{unimsstyle.cwl} aktivieren.
  \end{enumerate}
\end{frame}


\section{Zusätzliche Befehle}
\begin{frame}{Zusätzliche Befehle}
  Folgende Befehle stellt das Paket zusätzlich zur Verfügung:
  \begin{itemize}
    \item \mintinline{tex}|\code{...}|: Setzt Text in \code (ohne Syntax Highlighting).
    \item \mintinline{tex}|\socalled{...}|: Alternative \socalled{Anführungszeichen}, um Verwechslung mit Zitaten zu vermeiden.
    \item \mintinline{tex}|\hyper{URL}{text}|: Anklickbarer \hyper{https://de.wikipedia.org/wiki/Hyperlink}{Link}, unterstrichen und mit \code{themecolor} eingefärbt.
    \item \mintinline{tex}|\highlight<...>{...}|: \highlight<2->{Farbliche Hervorhebung} mit \code{themecolor}.
    \item \mintinline{tex}|\minor<...>{...}|: Leicht ausgegrauter \minor<2->{Text}.
  \end{itemize}
\end{frame}


\section{Krams zu Testzwecken}
\begin{frame}{Krams zu Testzwecken}
  \begin{itemize}
    \item Hyperlinks: \hyper{https://ctan.org/tex-archive/macros/latex/contrib/beamer/}{Beamer class user guide}
    \item Zitationen: \cite{biblatex-ctan}
    \item Inline-Mathe: $a^2 + b^2 = c^2$
  \end{itemize}
  
  \[
    x_{1,2} = - \frac{p}{2} \pm \sqrt{\left(\frac{p}{2}\right)^2 - q}
  \]
\end{frame}


\begin{frame}{Quellcode-Einbindung mit den generischen Befehlen}
  \textbf{Beispiel für Quellcode aus Datei:}
  \codefromfile{c}{programm.c}
  
  \textbf{Beispiel für Inline-C-Code:}
  \codeinline{c}{int main(int argc, char** argv)}
\end{frame}


\begin{frame}[fragile]{Quellcode-Einbindung mit den generischen Befehlen}
  \textbf{Beispiel für direkt eingegebenen C-Code:}
  \begin{itemize}
    \item Erfordert \texttt{fragile}-Option im Frame-Header.
  \end{itemize}

  \begin{codeblock}{c}
#include <stdio.h>

int main(int argc, char** argv){
  int i;
  for(i = 0; i < argc; i++){
    printf("%s \n", argv[i]);
  }
  return 0;
}
  \end{codeblock}
\end{frame}


\begin{frame}[c]{Blöcke}
  \begin{block}{block}
    Ein Block.
  \end{block}

  \begin{exampleblock}{exampleblock}
    Ein freundlicher Block.
  \end{exampleblock}
  
  \begin{alertblock}{alertblock}
    Ein boshafter Block.
  \end{alertblock}
\end{frame}


\begin{frame}{Literatur}
  \printbibliography
\end{frame}
\end{document}