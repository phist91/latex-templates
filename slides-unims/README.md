# Universität Münster Unofficial LaTeX Slide Template

This theme tries to incorporate some elements of the Universität Münster as defined by the Corporate Design Manual, while simultaneously aiming for a clean and not too overloaded layout.
Furthermore, this template does not require proprietary fonts, such as Meta.

## Preview

See [README.pdf](README.pdf) for a preview.

## Documentation
Work in progress.
Meanwhile, see [README.pdf](README.pdf) for (German) explanations of the package options.

## Changelog

#### Version 1.1.1 (2023-12-21)
- `\highlight`-Command now typesets in bold.
- Sections in table of contents are now in theme color.
- Caption names are now numbered by default an in theme color.

#### Version 1.1 (2023-09-24)
Added `nofooterbars` package option to suppress drawing of colored bars in the footer.

#### Version 1.0 (2023-09-21)
First major release.

#### Version 0.4 (2023-09-21)
Added README and word completion list for TeXstudio.

#### Version 0.3 (2023-09-20)
Added institute logo support and some custom commands.

#### Version 0.2 (2023-09-19)
Switched to key-value options using the kvoptions package and added support for source code syntax highlighting and bibliography references.

#### Version 0.1 (2023-09-18)
Initial version based on old wwustyle templates.