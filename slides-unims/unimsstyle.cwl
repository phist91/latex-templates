# Command completion list for unimsstyle.sty, version 1.1.

#include:ifthen
#include:tikz
#include:xspace
#include:babel
#include:csquotes
#include:biblatex
#include:xcolor
#ifOption:math
#include:mathtools
#include:amssymb
#include:wasysym
#endif
#ifOption:code=minted
#include:minted
#endif
#ifOption:code=listingsutf8
#include:listingsutf8
#endif

# Completion list for package keyvals
#keyvals:\usepackage/unimsstyle#c
themecolor=#Graphit,Pantone432,Cassis,Pantone234,Lapis,Pantone7462,Cyan,Pantone312,Ozean,Pantone315,Tuerkis,Pantone3282,Apfelgruen,Pantone369,Peridot,Pantone390,Lcinfo
unilogo=#none,%<file path%>
institutelogo=#none,%<file path%>
nofooterbars=#true,false
lang=#none,de,en
code=#none,listingsutf8,minted
bib=#none,bibtex,biber
bibfile=%<file path%>
intermediatetoc=#true,false
math=#true,false
#endkeyvals

\codefromfile{language%plain}{file}
\codeinline{language%plain}{code%plain}
\code{code%plain}
\begin{codeblock}{language%plain}#\lstlisting#V
\end{codeblock}

\socalled{text}
\highlight{text}
\highlight<overlay%plain>{text}
\minor{text}
\minor<overlay%plain>{text}
\minoritem<overlay%plain>{text}
\hyper{URL}{text}

# completion list for colors
unimscolorgraphit#B
unimscolorcassis#B
unimscolorlapis#B
unimscolorcyan#B
unimscolorozean#B
unimscolortuerkis#B
unimscolorapfelgruen#B
unimscolorperidot#B
unimscolorlcinfo#B
themecolor#B
textcolor#B
bgcolor#B
minorcolor#B
alertcolor#B
examplecolor#B