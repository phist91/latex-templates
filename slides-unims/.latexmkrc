$pdf_mode = 1;
$pdflatex = 'pdflatex -interaction=nonstopmode -shell-escape';
$clean_ext = "aux auxlock bbl bcf blg fdb_latexmk fls log nav out run.xml snm synctex.gz toc vrb";
$bibtex_use = 2;
@default_files = ('slides-unims.tex');