# Verwendung der Register
# $a0 .. Parameter n
# $v0 .. Return value

abs:
    bge  $a0,$zero,end_if # if (n>=0) goto endif
    sub  $v0,$zero,$a0    # returnvalue = 0 - n
    jr   $ra              # return (-n in $v0)
end_if:
    add  $v0,$a0,$zero    # returnvalue = n
    jr   $ra              # return (n in $v0)

#
# Testdaten. Zur Kontrolle: Ausgabe ist:
# 5220
#
    .data
n1: .word -5
n2: .word 22
n3: .word 0


#
# main
#
    .text
    .globl main

main:
    lw     $a0, n1        # load first test value
  
    jal    abs            # jump to subroutine
    
    move   $a0, $v0
    li     $v0, 1         # print result
    syscall
    
    lw     $a0, n2        # load second test value
  
    jal    abs            # jump to subroutine
    
    move   $a0, $v0
    li     $v0, 1         # print result
    syscall
    
    lw     $a0, n3        # load third test value
  
    jal    abs            # jump to subroutine
    
    move   $a0, $v0
    li     $v0, 1         # print result
    syscall
    
    li $v0, 10
    syscall                # exit

#
# end main
#
