start:     move   $v0,$zero
           move   $t0,$zero
           move   $t1,$a1
           bge    $t1,$zero,loop
           sub    $a0,$zero,$a0
           sub    $t1,$zero,$t1
           li     $t0,1
loop:      beq    $t1,$zero,end_loop
           add    $v0,$v0,$a0
           addi   $t1,$t1,-1
           j      loop
end_loop:  beq    $t0,$zero,final
           sub    $a0,$zero,$a0
final:     jr     $ra

           
#
# Testdaten. Zur Kontrolle: Ergebnis sollte -481 sein.
#
    .data
x:     .word 13
y:     .word -37


#
# main
#
    .text
    .globl main

main:
    lw    $a0, x
    lw    $a1, y  

    jal   start

    move  $a0, $v0    # copy result

    li    $v0, 1      # print result
    syscall

    li    $v0, 10
    syscall           # exit

#
# end main
#
