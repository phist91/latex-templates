# Übungszettelvorlage

Basierend auf der selbst erstellten Vorlage zur Vorlesung Rechnerstrukturen im Sommersemester 2020.

## Grundlegendes

- Für jeden zu erstellenden Zettel soll eine Datei `blattXX.tex` im Hauptverzeichnis des Repositorys liegen, wobei `XX` die Nummer des Zettels bezeichnet.
- In jeder Datei `blattXX.tex` müssen die Zeilen 4 und 5 die Schalter in Form von Booleschen Variablen zur Ausgabe der Lösung und des Bewertungsschemas enthalten. Das ist unflexibel und wird vielleicht mal durch Kommandozeilen-Parameter ersetzt.
- Damit alle Zettel ein einheitliches Layout haben, sollte die gesamte Präambel in der Datei `config/config.tex` angelegt werden und diese mittels `\input{config/config.tex}` in jeder `blattXX.tex` eingebunden werden.
- Die einzelnen Aufgaben sollten zwecks Modularisierung in eigenständigen Dateien im Ordner `aufgaben` liegen. Dies ist jedoch nicht verpflichtend.
- Jede Aufgabendatei enthält neben der Aufgabenstellung auch einen Lösungsvorschlag und ein Bewertungsschema, die jedoch nur ausgegeben werden, wenn die oben genannten Schalter aktiviert werden.

## Kompilieren

- *Einzelner Zettel:* Nutzung des Makefiles vermöge ```make nr=XX```, wobei ```XX``` die Nummer des Zettels bezeichnet, also zum Beispiel
    ```
        make nr=09
    ```
    für den neunten Zettel. Erzeugt wird dann der Zettel sowie die zugehörigen Lösungen bzw. Korrekturhilfen als PDF. Alternativ auch direktes Aufrufen des Shell-Skripts vermöge `./mkblatt XX`. Ggfs. muss `mkblatt` nach dem Klonen noch als ausführbar markiert werden vermöge `chmod +x mkblatt`.

- *Alle Zettel:* Alle Zettel können mittels ```make all``` erzeugt werden. Werden neue Zettel angelegt, müssen in der `Makefile` unter dem Target `pdf-all` entsprechende Zeilen hinzugefügt werden.
- *Aufräumen:* Temporäre LaTeX-Dateien im Hauptverzeichnis können mittels `make clean` entfernt werden. Möchte man zusätzlich die kompilierten Blätter entfernen, ist `make clean-hard` zu verwenden.

## Besondere TeX-Befehle
Die folgenden Befehle sind vordefiniert und können in der Präambel `config/config.tex` angepasst werden:

- ```\aufgabetitel{#1}{#2}```: Kopf für neue Aufgabe (wird automatisch fortgezählt). `#1` ist Titel der Aufgabe, `#2` die Gesamtpunktzahl der Aufgabe.
- ```\praesenztitel{#1}```: Kopf für neue Präsenzaufgabe (wird automatisch fortgezählt), die mit einem Stiftsymbol versehen wird und keine Punkte bringt. `#1` ist Titel der Aufgabe.
- ```\umbruchoriginal```: fügt einen Seitenumbruch ein, der ***nicht*** in der Version mit Lösung erscheint.
- ```\umbruchlsg```: fügt einen Seitenumbruch ein, der ***nur*** in der Version mit Lösung erscheint (Schalter `loesung` ist `true`).
- ```\umbruchbewertung```: fügt einen Seitenumbruch ein, der ***nur*** in der Version mit Bewertungsschema erscheint (Schalter `bewertung` ist `true`).
- ```\begin{loesung} ... \end{loesung}```: Alles, was in so einer Umgebung steht, wird nur in der Version mit Lösung  (Schalter `loesung` ist `true`) ausgegeben. Sollte also am Ende jeder großen Aufgabe zu finden sein, damit die Lösung unter der Aufgabenstellung ausgegeben wird. Ausgabe wird mit ***— Lösung Anfang —*** eingeleitet und ***— Lösung Ende —*** abgeschlossen; nach jeder Aufgabe erfolgt automatisch ein Seitenumbruch.
- ```\begin{bewertung} ... \end{bewertung}```: Alles, was in so einer Umgebung steht, wird nur in der Version mit Bewertungsschema (Schalter `bewertung` ist `true`) ausgegeben. Sollte also am Ende jeder großen Aufgabe zu finden sein, damit auf die Lösung auch direkt das Bewertungsschema folgt. Ausgabe wird mit ***— Bewertungsschema Anfang —*** eingeleitet und ***— Bewertungsschema Ende —*** abgeschlossen; nach jeder Aufgabe erfolgt automatisch ein Seitenumbruch.
- ```\iforiginal{#1}```: Parameter `#1` wird ***nicht*** in der Version mit Lösung ausgegeben. Eignet sich z.B., wenn die Aufgabenstellung eine Vorlage für eine Tabelle enthält, die in der Lösung nicht ausgegeben werden soll.
- ```\ifloesung{#1}```: Parameter `#1` wird nur in der Version mit Lösung ausgegeben. Eignet sich z.B., wenn man in der Lösungsversion statt einer Tabellenvorlage an gleicher Stelle die ausgefüllte Tabelle setzen lassen möchte.
- ```\ifbewertung{#1}```: Parameter `#1` wird nur in der Version mit Bewertungsschema ausgegeben. Eignet sich z.B., wenn man in der Korrekturhilfe an geeigneter Stelle Kommentare für Tutor\*innen ausgeben lassen möchte.

## Sonstige Hinweise
- PDF-Dateien werden durch die `.gitignore` normalerweise automatisch ausgeschlossen. Möchte man einzelne PDFs explizit zulassen (z.B. Vektorgrafiken), gelingt dies mittels eines forcierten Adds: `git add -f image.pdf`. Das ist natürlich nur relevant, wenn man die Vorlage in ein Git-Repository überträgt und dabei die `.gitignore` mit übernimmt.
