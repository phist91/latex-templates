$pdf_mode = 1;
$pdflatex = 'pdflatex -interaction=nonstopmode -shell-escape';
$clean_ext = "aux auxlock bbl bcf blg fdb_latexmk fls lof log lol out run.xml synctex.gz tdo toc";
$bibtex_use = 2;
@default_files = ('template.tex');